package com.example.calculator205150401111044

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        displayTop.setOnClickListener(this)
        displayBottom.setOnClickListener(this)
        c.setOnClickListener(this)
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        buttonAdd.setOnClickListener(this)
        buttonSubtract.setOnClickListener(this)
        buttonMultiply.setOnClickListener(this)
        buttonDivide.setOnClickListener(this)
        buttonEqual.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            c.id -> {
                displayTop.text = ""
                displayBottom.text = ""
            }
            button0.id -> if (displayTop.text.isNotEmpty()) displayTop.text =
                displayTop.text.toString().plus("0")
            button1.id -> displayTop.text = displayTop.text.toString().plus("1")
            button2.id -> displayTop.text = displayTop.text.toString().plus("2")
            button3.id -> displayTop.text = displayTop.text.toString().plus("3")
            button4.id -> displayTop.text = displayTop.text.toString().plus("4")
            button5.id -> displayTop.text = displayTop.text.toString().plus("5")
            button6.id -> displayTop.text = displayTop.text.toString().plus("6")
            button7.id -> displayTop.text = displayTop.text.toString().plus("7")
            button8.id -> displayTop.text = displayTop.text.toString().plus("8")
            button9.id -> displayTop.text = displayTop.text.toString().plus("9")
            buttonAdd.id -> {
                if (displayTop.text.isNotEmpty() && displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.toString().plus("+")
                else if (!displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.dropLast(1).toString() + '+'
            }
            buttonSubtract.id -> {
                if (displayTop.text.isNotEmpty() && displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.toString().plus("-")
                else if (!displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.dropLast(1).toString() + '-'
            }
            buttonMultiply.id -> {
                if (displayTop.text.isNotEmpty() && displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.toString().plus("x")
                else if (!displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.dropLast(1).toString() + 'x'
            }
            buttonDivide.id -> {
                if (displayTop.text.isNotEmpty() && displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.toString().plus("/")
                else if (!displayTop.text.last().isDigit())
                    displayTop.text = displayTop.text.dropLast(1).toString() + '/'
            }
            buttonEqual.id -> {
                if (displayTop.text.isNotEmpty()) {
                    val listOfNumbers: List<String> = displayTop.text.split('/', 'x', '+', '-')
                    val OPERATORS = setOf('x', '+', '-', '/')
                    val (operators, numbers) = displayTop.text.toList()
                        .partition { it in OPERATORS }
                    var result: Double = listOfNumbers[0].toDouble()
                    for (i in 1..operators.size) {
                        when (operators[i-1]) {
                            '+' -> result += listOfNumbers[i].toDouble()
                            '-' -> result -= listOfNumbers[i].toDouble()
                            'x' -> result *= listOfNumbers[i].toDouble()
                            '/' -> result /= listOfNumbers[i].toDouble()
                        }
                    }
                    displayBottom.text = result.toString()
                    val intent = Intent(this, Activity2::class.java).apply {
                        putExtra("result", "${displayTop.text}=${result.toString()}")
                    }
                    startActivity(intent)
                }
            }
        }
    }
}
